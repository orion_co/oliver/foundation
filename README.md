Providing an event service for creating and managing events, as well as participants.
Participants can indicate their participation level - undecided, maybe, yes, no.  

Idea inspired by a project I created in 2008 where my group of friends and I would go on mountain bike excursions weekly.  Frustrated by lack of effectivness in managing text messages (group messages weren't a feature), I created a basic service which allowed everyone to indicate whether or not they would be participating.  This made it easier to coordinate carpools, etc;

Today, I still see the need for a basic event service like this - perhaps something GroupMe could use - basic event management.  My kids are active in many sports and activities and there's still ongoing back and forth to coordinate participation.  